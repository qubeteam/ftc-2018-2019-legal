package org.firstinspires.ftc.teamcode.hardware

import com.qualcomm.robotcore.hardware.DcMotor
import com.qualcomm.robotcore.hardware.DcMotorSimple
import com.qualcomm.robotcore.hardware.HardwareMap
import kotlin.math.max
import kotlin.math.min

/**
 * Latching subsystem.
 *
 * This class controls the hardware for latching on / dropping from the lander.
 */

class Latcher (hwMap : HardwareMap) {
    val latchMotor = hwMap.dcMotor["latchMotor"] ?: throw Error("Failed to find motor latchMotor")
    val latchBlock = hwMap.servo["latchBlockServo"] ?: throw Error("Failed to find motor latchBlockServo")

    val START = 0
    val LIMIT = 0
    val MULTIPLIER = 50

    val latcherOpen = -10000
    var latcherPosition = 0

    var startMode = StartPosition.ClOSED

    enum class StartPosition {
        OPEN, ClOSED
    }

    init {
        latchMotor.direction = DcMotorSimple.Direction.REVERSE
        latchMotor.mode = DcMotor.RunMode.STOP_AND_RESET_ENCODER
        latchMotor.mode = DcMotor.RunMode.RUN_TO_POSITION
        latchMotor.zeroPowerBehavior = DcMotor.ZeroPowerBehavior.BRAKE
    }

    fun setStart(start : StartPosition){
        startMode = start
        if(startMode == StartPosition.OPEN) latchMotor.direction = DcMotorSimple.Direction.FORWARD
        else latchMotor.direction = DcMotorSimple.Direction.REVERSE

        block(start == StartPosition.OPEN)
    }

    fun block(pos : Boolean){
        latchBlock.position = when (pos){
            true -> 0.0
            false -> 0.5
        }
    }

    fun latch(power: Double) {
        latcherPosition += (power*MULTIPLIER).toInt()
        //latcherPosition = Math.max(START, Math.min(LIMIT, latcherPosition))

        val targetLatcherPosition = latcherPosition * when(startMode){
            StartPosition.OPEN -> -1
            StartPosition.ClOSED -> 1
        }

        runLatchToPosition(targetLatcherPosition, 1.0)
    }

    fun runLatchToPosition(position: Int, power: Double) {
        latchMotor.targetPosition = position
        latchMotor.power = power
    }

    fun stop() {
        latchMotor.power = 0.0
    }

}

