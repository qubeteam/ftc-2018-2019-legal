package org.firstinspires.ftc.teamcode.tests

import com.qualcomm.robotcore.eventloop.opmode.Autonomous
import com.qualcomm.robotcore.hardware.ColorSensor
import com.qualcomm.robotcore.hardware.DistanceSensor
import org.firstinspires.ftc.robotcore.external.navigation.DistanceUnit
import org.firstinspires.ftc.teamcode.OpMode
import org.firstinspires.ftc.teamcode.hardware.Hardware

@Autonomous(name = "Color Sensor Test", group = "Tests")
class ColorSensorTest: OpMode() {
    override fun Hardware.run() {
        val colorSensor = hardwareMap.get(ColorSensor::class.java, "colorDistanceSensor")
        val distanceSensor = hardwareMap.get(DistanceSensor::class.java, "colorDistanceSensor")
        waitForStart()
        while(opModeIsActive()) {
            telemetry.addData("Distance", "%f cm", distanceSensor.getDistance(DistanceUnit.CM))
            telemetry.update()
        }
    }
}
