package org.firstinspires.ftc.teamcode.autonomy

import com.qualcomm.robotcore.eventloop.opmode.Autonomous
import com.qualcomm.robotcore.util.ElapsedTime
import org.firstinspires.ftc.teamcode.hardware.Hardware
import org.firstinspires.ftc.teamcode.hardware.sensors.TFOD
import org.firstinspires.ftc.teamcode.waitMillis

@Autonomous
class AutonomyNoParking : AutonomyBase() {
    val timer = ElapsedTime()

    override fun preInit() {
    }

    override fun preInitLoop() {
        val mata = tfod.gold
        if(mata != TFOD.Positions.UNKNOWN) {
            goldPosition = mata
            waitMillis(50)
        }
        telemetry.addData("GoldPos", goldPosition)
        telemetry.addLine("READY")
        telemetry.update()
    }

    override fun Hardware.run() {
        land()

        timer.reset()

        removeGold(Basis.SLIDERS)
        tfod.stop()

        followTrajectory(arrayListOf(Pair(120.0, 25.0), Pair(0.0, -45.0), Pair(100.0, -45.0), Pair(0.0, 135.0)))
        dropMarker()
        followTrajectory(arrayListOf(Pair(50.0, 135.0)))
//        followTrajectory(arrayListOf(Pair(-120.0, 45.0)))
//
//        followTrajectory(arrayListOf(Pair(0.0, 45.0)))
//        dropMarker()

//        followTrajectory(arrayListOf(Pair(120.0, 45.0)))
//        intake.runSlideToPosition(intake.SLIDER_OPEN/2,0.8)
//        val start = timer.milliseconds()
//        while (timer.milliseconds() - start < 1000 && intake.slideMotor.isBusy) idle();
    }
}
