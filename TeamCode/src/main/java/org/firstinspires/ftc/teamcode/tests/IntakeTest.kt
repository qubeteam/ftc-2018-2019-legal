package org.firstinspires.ftc.teamcode.tests

import com.qualcomm.robotcore.eventloop.opmode.TeleOp
import com.qualcomm.robotcore.hardware.DcMotor
import org.firstinspires.ftc.teamcode.Gamepad
import org.firstinspires.ftc.teamcode.OpMode
import org.firstinspires.ftc.teamcode.hardware.Hardware
import org.firstinspires.ftc.teamcode.hardware.Intake

@TeleOp (name = "Intake Test", group = "Tests")
class IntakeTest: OpMode() {

    override fun Hardware.run() {
        val gp1 = Gamepad(gamepad1)

        val limit = 0.8


        waitForStart()

        while (opModeIsActive()) {

            intake.slideMotor.mode = DcMotor.RunMode.RUN_WITHOUT_ENCODER
            intake.slideMotor.zeroPowerBehavior = DcMotor.ZeroPowerBehavior.BRAKE

            intake.slideMotor.power = Math.min(Math.max(-limit, (gp1.right_trigger - gp1.left_trigger).toDouble()), limit)

            //intake.moveSlider((gp1.right_trigger - gp1.left_trigger).toDouble())

            if (gp1.checkHold(Gamepad.Button.A))  intake.rotate(Intake.ModeRotate.CLOSE)
            else if (gp1.checkHold(Gamepad.Button.B)) intake.rotate(Intake.ModeRotate.OPEN)
            else if (gp1.checkHold(Gamepad.Button.Y)) intake.rotate(Intake.ModeRotate.CLOSE)

            if (gp1.left_stick_y > 0.5) intake.maturica(Intake.ModeMaturica.IN)
            else if (gp1.left_stick_y <= 0.5 && gp1.left_stick_y >= -0.5) intake.maturica(Intake.ModeMaturica.STOP)
            else if (gp1.left_stick_y < -0.5) intake.maturica(Intake.ModeMaturica.OUT)

            telemetry.addData("Slider position", intake.slideMotor.currentPosition)
            telemetry.addData("Target", intake.pos)
            telemetry.addData("Y", gp1.left_stick_y)
            telemetry.update()
        }


    }
}
