package org.firstinspires.ftc.teamcode.tests

import com.qualcomm.robotcore.eventloop.opmode.Autonomous
import com.qualcomm.robotcore.hardware.DcMotor
import org.firstinspires.ftc.teamcode.Gamepad
import org.firstinspires.ftc.teamcode.OpMode
import org.firstinspires.ftc.teamcode.hardware.Hardware

@Autonomous(name = "Encoder test", group = "Tests")
class EncoderTest: OpMode() {
    override fun Hardware.run() {
        val motor = hardwareMap.get(DcMotor::class.java, "leftMotorEncoder")
        motor.mode = DcMotor.RunMode.STOP_AND_RESET_ENCODER
        motor.mode = DcMotor.RunMode.RUN_USING_ENCODER

        while (opModeIsActive()) {
            telemetry.addData("Position", motor.currentPosition)
            telemetry.update()
        }
    }
}
