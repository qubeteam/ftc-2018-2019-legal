package org.firstinspires.ftc.teamcode.tests

import com.qualcomm.robotcore.eventloop.opmode.TeleOp
import org.firstinspires.ftc.teamcode.Gamepad
import org.firstinspires.ftc.teamcode.OpMode
import org.firstinspires.ftc.teamcode.hardware.Hardware

@TeleOp(name = "TranslationTest", group = "Tests")
public class TranslationTest : OpMode() {

    override fun Hardware.run() {
        var currentPosition = 0.0
        val gp1 = Gamepad(gamepad1)
        val value = 10

        waitForStart()

        while (opModeIsActive()) {
            if (gp1.checkToggle(Gamepad.Button.A)) currentPosition += value
            if (gp1.checkToggle(Gamepad.Button.B)) currentPosition -= value
            if (gp1.checkToggle(Gamepad.Button.Y)) {
                goTo(currentPosition, 0.0)
                currentPosition = 0.0
            }
            telemetry.addData("Target", currentPosition)
            telemetry.update()
        }
    }
}
