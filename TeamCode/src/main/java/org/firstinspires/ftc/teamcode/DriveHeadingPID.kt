package org.firstinspires.ftc.teamcode

object DriveHeadingPID {
     var p: Double = 1.9
     var i: Double = 0.3
     var d: Double = 1.0
}
