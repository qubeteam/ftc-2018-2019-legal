package org.firstinspires.ftc.teamcode.tests

import com.qualcomm.robotcore.eventloop.opmode.Autonomous
import org.firstinspires.ftc.teamcode.OpMode
import org.firstinspires.ftc.teamcode.hardware.Hardware

@Autonomous
class DistanceSensorTest : OpMode() {
    override fun Hardware.run() {
        waitForStart()
        while(opModeIsActive()) {
            telemetry.addData("Distance","%.2f", distanceSensor.distance)
            telemetry.update()
        }
    }
}
