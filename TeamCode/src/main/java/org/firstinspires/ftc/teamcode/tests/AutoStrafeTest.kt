package org.firstinspires.ftc.teamcode.tests

import com.qualcomm.robotcore.eventloop.opmode.Autonomous
import com.qualcomm.robotcore.util.ElapsedTime
import org.firstinspires.ftc.teamcode.Gamepad
import org.firstinspires.ftc.teamcode.OpMode
import org.firstinspires.ftc.teamcode.StrafePID
import org.firstinspires.ftc.teamcode.hardware.Hardware


object pula {
     var constant = 1000.0
}
@Autonomous
class AutoStrafeTest : OpMode() {
    override fun Hardware.run() {
        val timer = ElapsedTime()
        motors.runWithConstantVelocity()
        waitForStart()
        timer.reset()

        var lastDist = 0.0
        while(opModeIsActive()) {
            if (StrafePID.dist == lastDist) idle()
            lastDist = StrafePID.dist
            strafe(StrafePID.dist, 0.0, maxTimePerStrafe)
        }
    }
}
