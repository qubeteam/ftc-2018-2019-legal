package org.firstinspires.ftc.teamcode.hardware

import com.qualcomm.robotcore.hardware.DcMotor
import com.qualcomm.robotcore.hardware.DcMotorSimple
import com.qualcomm.robotcore.hardware.HardwareMap
import java.io.File
import java.io.FileOutputStream
import java.io.PrintWriter
import java.util.*
import kotlin.math.absoluteValue

/**
 * OutTake subsystem.
 *
 * This class controls the hardware for placing minerals in the lander.
 */
class OutTake(hwMap: HardwareMap) {
    companion object {
        const val SLIDER_OPEN = 550
        const val SLIDER_CLOSE = 0
        var SLIDER_START_POSITION = 0
        const val MULTIPLIER = 20
        const val SLIDER_MARKER = 150

        const val SERVO_BLOCK_OPEN = 0.7
        const val SERVO_BLOCK_CLOSE = 0.2

        const val SERVO_TRANSFER_CLOSE = 0.3
        const val SERVO_TRANSFER_OPEN = 0.1

        val file = File("sdcard/FIRST/Cache", "outTake.txt")
    }

    var lastPosition = 0

    val outTakeSlider = hwMap.dcMotor["outTakeSlider"]
            ?: throw Exception("Failed to find motor outTakeSlider")

    var outTakePosition: Int = 0

    val block = hwMap.servo["outTakeServo"] ?: throw Exception("Failed to find servo outTakeServo")

    val transferBlock = hwMap.servo["transferServo"] ?: throw Exception("Failed to find transferServo")

    init {
        outTakeSlider.zeroPowerBehavior = DcMotor.ZeroPowerBehavior.FLOAT
        outTakeSlider.direction = DcMotorSimple.Direction.REVERSE
        outTakeSlider.mode = DcMotor.RunMode.STOP_AND_RESET_ENCODER
        outTakeSlider.mode = DcMotor.RunMode.RUN_USING_ENCODER

        outTakeSlider.power = 0.0
        outTakePosition = 0
    }

    fun openSlider() {
        outTakePosition = SLIDER_OPEN
        outTakeSlider.targetPosition = outTakePosition
        outTakeSlider.mode = DcMotor.RunMode.RUN_TO_POSITION
        outTakeSlider.power = 1.0
    }

    fun closeSlider() {
        outTakePosition = SLIDER_CLOSE
        outTakeSlider.targetPosition = outTakePosition
        outTakeSlider.mode = DcMotor.RunMode.RUN_TO_POSITION
        outTakeSlider.power = 1.0
    }

    fun openMarker() {
        outTakePosition = SLIDER_MARKER
        outTakeSlider.targetPosition = outTakePosition
        outTakeSlider.mode = DcMotor.RunMode.RUN_TO_POSITION
        outTakeSlider.power = 1.0
    }

    fun moveSlider(power: Double) {
        outTakePosition += (power * MULTIPLIER).toInt()
        outTakePosition += SLIDER_START_POSITION

        outTakeSlider.targetPosition =outTakePosition
        outTakeSlider.mode = DcMotor.RunMode.RUN_TO_POSITION
        outTakeSlider.power = 0.8
    }

    fun dropMinerals(drop: Boolean) {
        block.position = when (drop) {
            true -> SERVO_BLOCK_OPEN

            false -> SERVO_BLOCK_CLOSE
        }
    }

    fun stop() {
        outTakeSlider.power = 0.0
    }

    fun close() {
        block.position = SERVO_BLOCK_CLOSE
        outTakeSlider.targetPosition = SLIDER_CLOSE
        outTakeSlider.mode = DcMotor.RunMode.RUN_TO_POSITION
        outTakeSlider.power = 1.0
    }


    fun blockTransfer() {
        transferBlock.position = when(outTakePosition != 0 || outTakeSlider.currentPosition > 3) {
            true -> SERVO_TRANSFER_CLOSE
            false -> SERVO_TRANSFER_OPEN
        }
    }
}
