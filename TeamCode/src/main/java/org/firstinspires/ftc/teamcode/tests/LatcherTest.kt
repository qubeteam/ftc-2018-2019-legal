package org.firstinspires.ftc.teamcode.tests

import com.qualcomm.robotcore.eventloop.opmode.TeleOp
import org.firstinspires.ftc.teamcode.Gamepad
import org.firstinspires.ftc.teamcode.OpMode
import org.firstinspires.ftc.teamcode.hardware.Hardware
import org.firstinspires.ftc.teamcode.hardware.Latcher

@TeleOp (name = "LatcherTest", group = "Tests")

class LatcherTest: OpMode() {

    override fun Hardware.run() {
        val gp1 = Gamepad(gamepad1)
        var position = 0.0;
        waitForStart()

        while (opModeIsActive()) {
            latcher.latch((gp1.right_trigger - gp1.left_trigger).toDouble())
            telemetry.addData("Target", latcher.latcherPosition)
            telemetry.addData("Position", latcher.latchMotor.currentPosition)
            telemetry.update()
        }
    }
}
