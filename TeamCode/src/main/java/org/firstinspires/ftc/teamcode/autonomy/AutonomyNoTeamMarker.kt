package org.firstinspires.ftc.teamcode.autonomy

import com.qualcomm.robotcore.eventloop.opmode.Autonomous
import com.qualcomm.robotcore.util.ElapsedTime
import org.firstinspires.ftc.teamcode.hardware.Hardware
import org.firstinspires.ftc.teamcode.hardware.Intake
import org.firstinspires.ftc.teamcode.hardware.sensors.TFOD
import org.firstinspires.ftc.teamcode.waitMillis

@Autonomous
class AutonomyNoTeamMarker : AutonomyBase() {
    val timer = ElapsedTime()

    override fun preInit() {
    }

    override fun preInitLoop() {
        val mata = tfod.gold
        if(mata != TFOD.Positions.UNKNOWN) {
            goldPosition = mata
            waitMillis(50)
        }
        telemetry.addData("GoldPos", goldPosition)
        telemetry.addLine("READY")
        telemetry.update()
    }

    override fun Hardware.run() {
        land()

        timer.reset()

        removeGold(Basis.SLIDERS)
        tfod.stop()

        followTrajectory(arrayListOf(Pair(30.0, 90.0)))

        dropMarker()

        intake.runSlideToPosition(intake.SLIDER_OPEN/2,0.8)
        var start = timer.milliseconds()
        while (timer.milliseconds() - start < 2000 && intake.slideMotor.isBusy) idle()
    }
}
