package org.firstinspires.ftc.teamcode.hardware

import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode
import com.qualcomm.robotcore.hardware.DistanceSensor
import com.qualcomm.robotcore.hardware.HardwareMap
import org.firstinspires.ftc.teamcode.hardware.sensors.DistanceColorSensor
import org.firstinspires.ftc.teamcode.hardware.sensors.Imu
import org.firstinspires.ftc.teamcode.hardware.sensors.REVDistanceSensor

class Hardware(hwMap: HardwareMap) {
    val motors = DriveMotors(hwMap)
    val intake = Intake(hwMap)
    val outTake = OutTake(hwMap)
    val latcher = Latcher(hwMap)
    val imu = Imu(hwMap)
    val distanceSensor = REVDistanceSensor(hwMap)
    val colorSensor = DistanceColorSensor(hwMap)
    //val marker = Marker(hwMap)

    fun stop() {
        motors.stop()
        intake.stop()
        outTake.stop()
        latcher.stop()
    }
}
