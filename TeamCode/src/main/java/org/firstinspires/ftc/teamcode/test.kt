package org.firstinspires.ftc.teamcode

import com.qualcomm.robotcore.eventloop.opmode.Autonomous
import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode
import java.io.File
import java.io.FileOutputStream

@Autonomous (name = "TEST")
class test : LinearOpMode(){
    override fun runOpMode() {

        val file = File("sdcard/FIRST/Cache/test.txt")
        val writer = FileOutputStream(file)

        waitForStart()

        while (opModeIsActive()){
            writer.write((100).toString().toByteArray())
        }

        writer.close()
    }
}
