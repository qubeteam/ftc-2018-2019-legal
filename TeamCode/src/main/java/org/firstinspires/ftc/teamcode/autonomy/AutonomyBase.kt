package org.firstinspires.ftc.teamcode.autonomy

import com.qualcomm.robotcore.util.ElapsedTime
import org.firstinspires.ftc.teamcode.OpMode
import org.firstinspires.ftc.teamcode.hardware.Intake
import org.firstinspires.ftc.teamcode.hardware.sensors.TFOD
import org.firstinspires.ftc.teamcode.waitMillis
import ro.cnmv.qube.ftc.autonomy.Values

abstract class AutonomyBase : OpMode() {

    val tfod: TFOD by lazy {
        TFOD(hardwareMap, telemetry)
    }

    var goldPosition = TFOD.Positions.UNKNOWN

    enum class Basis{
        SLIDERS, MOVEMENT
    }

    override fun preInit() {

    }

    override fun preInitLoop() {
        goldPosition = tfod.gold
        when (goldPosition){
            TFOD.Positions.UNKNOWN -> telemetry.addLine("GOLD NOT FOUND")
            else -> telemetry.addData("Gold Position", goldPosition)
        }
        telemetry.update()
    }

    protected fun followTrajectory(steps: ArrayList<Pair<Double, Double>>){
        steps.forEach { follow(it) }
    }

    private fun follow(step: Pair<Double, Double>) {
        val (distance, heading) = step
        goTo(distance, heading)
    }

    private fun extend(state: Boolean){
        hw.intake.autoExtend(state)
        val timer = ElapsedTime()
        while (hw.intake.slideMotor.isBusy && timer.milliseconds() < 1000);
    }

    fun removeGold(mode : Basis) {

        val target = when(goldPosition){
            TFOD.Positions.LEFT -> Values.INITIAL_LEFT
            TFOD.Positions.CENTER -> Values.INITIAL_CENTER
            else -> Values.INITIAL_RIGHT
        }

        val (angle, distance) = target

        followTrajectory(arrayListOf(Pair(0.0, angle)))

        if (mode == Basis.SLIDERS) {
            hw.intake.transfer(Intake.ModeTransfer.CLOSE)
            hw.intake.rotate(Intake.ModeRotate.OPEN)
            waitMillis(250)
            hw.intake.maturica(Intake.ModeMaturica.IN)
            extend(true)
            extend(false)
            hw.intake.rotate(Intake.ModeRotate.CLOSE)
            waitMillis(150)
            hw.intake.maturica(Intake.ModeMaturica.STOP)
        }
        else{
            followTrajectory(arrayListOf(
                    Pair(distance, 0.0),
                    Pair(-distance, 0.0)
            ))
        }
    }

    fun removeTeamMateGold(mode : Basis) {
        val target = when(goldPosition){
            TFOD.Positions.LEFT -> Values.TEAMMATE_LEFT
            TFOD.Positions.CENTER -> Values.TEAMMATE_CENTER
            else -> Values.TEAMMATE_RIGHT
        }

        val (angle, distance) = target

        followTrajectory(arrayListOf(Pair(0.0, angle)))

        if (mode == Basis.SLIDERS) {
            hw.intake.rotate(Intake.ModeRotate.OPEN)
            waitMillis(250)
            hw.intake.transfer(Intake.ModeTransfer.CLOSE)
            hw.intake.maturica(Intake.ModeMaturica.IN)
            extend(true)
            extend(false)
            hw.intake.rotate(Intake.ModeRotate.CLOSE)
            waitMillis(150)
            hw.intake.maturica(Intake.ModeMaturica.STOP)
        }
        else{
            followTrajectory(arrayListOf(
                    Pair(distance, 0.0),
                    Pair(-distance, 0.0)
            ))
        }
    }

    fun dropMarker() {
        hw.outTake.openMarker()
        val timer = ElapsedTime()
        while (hw.outTake.outTakeSlider.isBusy && timer.milliseconds() < 500);


        val startTime = timer.milliseconds()
        while (timer.milliseconds() - startTime < 700 && opModeIsActive()) hw.outTake.dropMinerals(true)
        hw.outTake.dropMinerals(false)
        hw.outTake.closeSlider()
    }

    fun land(){
        hw.latcher.block(false)
        hw.latcher.runLatchToPosition(hw.latcher.latcherOpen, 0.8)
        while (hw.latcher.latchMotor.isBusy) {
            telemetry.addLine("Landing")
            idle()
        }

        hw.latcher.block(true)
        waitMillis(100)
        goTo(Values.LAND_DISTANCE, 0.0)

        strafeTime(0.7, 90.0, 0.0, Values.LAND_STRAFE)
    }

}
