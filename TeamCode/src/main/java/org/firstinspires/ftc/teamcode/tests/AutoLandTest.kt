package org.firstinspires.ftc.teamcode.tests

import com.qualcomm.robotcore.eventloop.opmode.Autonomous
import org.firstinspires.ftc.teamcode.autonomy.AutonomyBase
import org.firstinspires.ftc.teamcode.hardware.Hardware

@Autonomous
class AutoLandTest : AutonomyBase() {

    override fun Hardware.run() {
        land()
    }
}
