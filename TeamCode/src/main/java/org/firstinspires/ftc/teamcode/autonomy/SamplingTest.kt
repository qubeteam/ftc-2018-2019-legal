package org.firstinspires.ftc.teamcode.autonomy

import com.qualcomm.robotcore.eventloop.opmode.Autonomous
import org.firstinspires.ftc.teamcode.hardware.Hardware
import org.firstinspires.ftc.teamcode.hardware.sensors.TFOD

@Autonomous(name = "Sampling Test", group = "tests")
public class SamplingTest : AutonomyBase() {
    override fun Hardware.run() {

        waitForStart()

        var currentPosition = TFOD.Positions.UNKNOWN
        while (opModeIsActive()) {
            var now = tfod.isGold()

            val pos = when(currentPosition) {
                TFOD.Positions.LEFT -> "Left"
                TFOD.Positions.RIGHT -> "Right"
                TFOD.Positions.CENTER -> "Center"
                else -> "UNKNOWN"
            }

            telemetry.update()
            sleep(1000)

            telemetry.addData("Position", pos)
            telemetry.update()
        }

        tfod.stop()
    }
}
