package org.firstinspires.ftc.teamcode.hardware

import com.qualcomm.robotcore.hardware.*
import java.io.File
import java.io.FileOutputStream
import java.io.PrintWriter
import java.util.*
import kotlin.NoSuchElementException

/**
* Intake motors and servos subsystem
*
* This class controls the hardware which takes the minerals
 */

class Intake (hwMap: HardwareMap) {
    val slideMotor = hwMap.dcMotor.get("intakeSlideMotor")
            ?: throw Exception("Failed to find motor intakeSlideMotor")
    val maturicaMotor = hwMap.dcMotor.get("intakeSweeperMotor")
            ?: throw Exception("Failed to find motor intakeSweeperMotor")
    val intakeServo1 = hwMap.servo.get("intakeServo1")
            ?: throw Exception("Failed to find servo intakeServo1")
    val intakeServo2 = hwMap.servo.get("intakeServo2")
            ?: throw Exception("Failed to find servo intakeServo2")
    val block = hwMap.servo.get("intakeBlock")
            ?: throw  Exception("Failed to find servo intakeBlock")

    val LEFT_MAX_POSITION = 0.33
    val LEFT_MIN_POSITION = 1.0

    val RIGHT_MAX_POSITION = 0.83
    val RIGHT_MIN_POSITION = 0.0

    init {
        slideMotor.direction = DcMotorSimple.Direction.REVERSE
        slideMotor.zeroPowerBehavior = DcMotor.ZeroPowerBehavior.BRAKE
        stopSlide()

        slideMotor.mode = DcMotor.RunMode.STOP_AND_RESET_ENCODER
        slideMotor.mode = DcMotor.RunMode.RUN_USING_ENCODER

        maturicaMotor.direction = DcMotorSimple.Direction.FORWARD
        maturicaMotor.zeroPowerBehavior = DcMotor.ZeroPowerBehavior.BRAKE
        stopRotate()

        //intakeServo1.scaleRange(LEFT_MIN_POSITION, LEFT_MAX_POSITION)
        //intakeServo2.scaleRange(RIGHT_MIN_POSITION, RIGHT_MAX_POSITION)

        //TODO: De-comment once the restraints have been set

        intakeServo1.direction = Servo.Direction.FORWARD
        intakeServo2.direction = Servo.Direction.FORWARD
        stopMaturica()


    }

    /// Slide Motor functions
    //---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    var pos = 0
    val MULTIPLIER: Int = 30
    val SLIDER_CLOSE: Int = 0
    val SLIDER_OPEN: Int = -1200
    val BUFFER: Int = 10
    val AUTO_POWER: Double = 1.0

    var SLIDER_START_POSITION = 0


    fun runSlideToPosition(position: Int, power: Double) {
        slideMotor.targetPosition = position
        slideMotor.mode = DcMotor.RunMode.RUN_TO_POSITION
        slideMotor.power = power
    }

    fun moveSlider(power: Double) {
        pos += (power * MULTIPLIER).toInt()
        pos += SLIDER_START_POSITION
        pos = Math.min(pos, SLIDER_OPEN)
        pos = Math.max(pos, SLIDER_CLOSE)

        runSlideToPosition(pos, 0.8)
    }

    fun stopSlide() {
        slideMotor.power = 0.0
    }

    fun getIntakePosiiton(): Int {
        return slideMotor.currentPosition
    }

    fun autoExtend(state: Boolean) {
        val poz = when (state) {
            true -> SLIDER_OPEN - BUFFER
            false -> SLIDER_CLOSE + BUFFER
        }
        runSlideToPosition(poz, AUTO_POWER)
    }

    fun runWithPower(power: Double) {
        slideMotor.mode = DcMotor.RunMode.RUN_USING_ENCODER
        slideMotor.power = power
    }

    //---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------


    /// Rotate Servos functions
    //---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

    fun setRotatePosition(pos: Pair<Double, Double>) {
        intakeServo1.position = pos.first
        intakeServo2.position = pos.second
    }

    fun stopRotate() {
        rotate(ModeRotate.CLOSE)
    }

    enum class ModeRotate {
        OPEN,
        CLOSE
    }

    enum class ModeTransfer {
        OPEN,
        CLOSE
    }

    fun rotate(mode: ModeRotate) {
        val (leftPos, rightPos) = when (mode) {
            ModeRotate.OPEN -> Pair(LEFT_MAX_POSITION, RIGHT_MAX_POSITION)
            ModeRotate.CLOSE -> Pair(LEFT_MIN_POSITION, RIGHT_MIN_POSITION)
        }
        setRotatePosition(Pair(leftPos, rightPos))
    }

    fun transfer(mode: ModeTransfer) {
        block.position = when (mode) {
            ModeTransfer.CLOSE -> 0.25
            ModeTransfer.OPEN -> 1.0
        }
    }


    //---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------


    /// Maturica functions
    //---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

    fun setMaturicaPower(power: Double) {
        maturicaMotor.power = power
    }

    fun stopMaturica() {
        setMaturicaPower(0.0)
    }

    enum class ModeMaturica {
        IN,
        OUT,
        STOP
    }

    val POWER = 1.0
    fun maturica(mode: ModeMaturica) {
        val power = when (mode) {
            ModeMaturica.IN -> POWER
            ModeMaturica.OUT -> -POWER
            ModeMaturica.STOP -> 0.0
        }
        setMaturicaPower(power)
    }

    //----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

    fun stop() {
        stopSlide()
        stopRotate()
        stopMaturica()
    }

}
