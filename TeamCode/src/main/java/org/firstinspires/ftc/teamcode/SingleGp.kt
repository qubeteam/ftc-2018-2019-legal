package org.firstinspires.ftc.teamcode

import com.qualcomm.robotcore.eventloop.opmode.TeleOp
import com.qualcomm.robotcore.util.ElapsedTime
import org.firstinspires.ftc.teamcode.hardware.Hardware
import org.firstinspires.ftc.teamcode.hardware.Intake
import org.firstinspires.ftc.teamcode.hardware.Latcher
import java.lang.Math.atan2
import kotlin.math.absoluteValue

@TeleOp(name = "SingleGp", group = "Main")
class SingleGp: OpMode() {
    override fun Hardware.run() {
        val gp = Gamepad(gamepad1)
        var isTransfer = true
        var rotate = false
        var block = true
        var endGame = false
        var sweepStart = 0
        var rotateTime = 0
        val timer = ElapsedTime()
        val TRANSF_THRESHOLD = 100
        latcher.setStart(Latcher.StartPosition.OPEN)
        latcher.block(true)

        waitForStart()

        while(opModeIsActive()) {
            if (gp.checkToggle(Gamepad.Button.START)) endGame = !endGame
            if(endGame) {
                telemetry.addLine("Endgame")
            } else {
                telemetry.addLine("Normal")
            }
            // OutTake
            if(endGame) {
                outTake.close()
            } else {
                if(gamepad1.right_bumper) {
                    outTake.openSlider()
                } else if (gamepad1.left_bumper) {
                    outTake.closeSlider()
                }
                outTake.dropMinerals(gp.checkHold(Gamepad.Button.B))

                telemetry.addData("Outtake slider", outTake.outTakeSlider.currentPosition)
            }

            outTake.blockTransfer()

            //Intake
           // intake.moveSlider((gp1.right_trigger - gp1.left_trigger).toDouble())
            val intakePower = (gp.left_trigger - gp.right_trigger).toDouble()

            intake.runWithPower(intakePower)


            if (gp.checkToggle(Gamepad.Button.A)) {
                rotate = !rotate
                rotateTime = timer.milliseconds().toInt()
            }

            if (rotate) intake.rotate(Intake.ModeRotate.OPEN)
            else intake.rotate(Intake.ModeRotate.CLOSE)

            telemetry.addData("Current Distance", colorSensor.getDistanceCm())
            if (rotate) {
                if (gp.left_stick_y > 0.5) intake.maturica(Intake.ModeMaturica.IN)
                else if (gp.left_stick_y.absoluteValue < 0.5) intake.maturica(Intake.ModeMaturica.STOP)
                else if (gp.left_stick_y < -0.5) intake.maturica(Intake.ModeMaturica.OUT)
                intake.transfer(Intake.ModeTransfer.CLOSE)
            } else if(gp.left_stick_y.absoluteValue < 0.5) {
                sweepStart = timer.milliseconds().toInt()
                intake.maturica(Intake.ModeMaturica.STOP)
            }
            else {
                if(timer.milliseconds().toInt() - rotateTime > 500) intake.transfer(Intake.ModeTransfer.OPEN)
                if(timer.milliseconds().toInt() - sweepStart > TRANSF_THRESHOLD) {
                    if (gp.left_stick_y > 0.0) intake.maturica(Intake.ModeMaturica.IN)
                    else intake.maturica(Intake.ModeMaturica.OUT)
                }
            }


            telemetry.addData("Intake slider", intake.getIntakePosiiton())

            // Latching
            if(endGame) {
                latcher.latch((gp.right_trigger - gp.left_trigger).toDouble())

                if (gp.checkToggle(Gamepad.Button.Y)) block = !block

            }

            telemetry.addData("Latcher", latcher.latchMotor.currentPosition)
            latcher.block(block)

            //Drive
            if (!endGame) {
                hw.motors.move(direction, speed, rotation/2)
            }
            else{
                hw.motors.move(direction, speed, rotation)
            }
            //Telemetry

            telemetry.update()
        }
    }

    /// The direction in which the robot is translating.
    private val direction: Double
        get() {
            val x = gamepad1.right_stick_x.toDouble()
            val y = -gamepad1.right_stick_y.toDouble()

            return atan2(y, x) / Math.PI * 180.0 - 90.0
        }

    /// Rotation around the robot's Z axis.
    private val rotation: Double
        get() = -gamepad1.left_stick_x.toDouble()

    /// Translation speed.
    private val speed: Double
        get() {
            val x = gamepad1.right_stick_x.toDouble()
            val y = gamepad1.right_stick_y.toDouble()

            return Math.sqrt((x * x) + (y * y))
        }
}
