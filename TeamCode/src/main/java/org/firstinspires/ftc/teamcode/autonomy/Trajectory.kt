package org.firstinspires.ftc.teamcode.autonomy

import kotlin.collections.ArrayList

object Trajectory {
     var distance1 = 0.0
     var heading1 = 0.0
     var distance2 = 0.0
     var heading2 = 0.0
     var distance3 = 0.0
     var heading3 = 0.0
     var distance4 = 0.0
     var heading4 = 0.0
     var distance5 = 0.0
     var heading5 = 0.0

    fun asArray() : ArrayList<Pair<Double, Double>>
            = arrayListOf(
                Pair(distance1, heading1),
                Pair(distance2, heading2),
                Pair(distance3, heading3),
                Pair(distance4, heading4),
                Pair(distance5, heading5))

    fun reversed() : ArrayList<Pair<Double, Double>>
            = arrayListOf(
            Pair(-distance5, heading5),
            Pair(-distance4, heading4),
            Pair(-distance3, heading3),
            Pair(-distance2, heading2),
            Pair(-distance1, heading1))
}
