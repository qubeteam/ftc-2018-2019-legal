package org.firstinspires.ftc.teamcode.tests

import android.text.style.UpdateAppearance
import com.qualcomm.robotcore.eventloop.opmode.TeleOp
import org.firstinspires.ftc.teamcode.Gamepad
import org.firstinspires.ftc.teamcode.OpMode
import org.firstinspires.ftc.teamcode.RotatePID
import org.firstinspires.ftc.teamcode.hardware.Hardware
import org.firstinspires.ftc.teamcode.waitMillis

@TeleOp(name = "RotationTest", group = "Tests")
public class RotationTest : OpMode() {

    override fun Hardware.run() {
        waitForStart()

        var lastAngle = 0.0
        while (opModeIsActive()) {
            rotateTo(RotatePID.angle)
            while (RotatePID.angle == lastAngle) idle()
            lastAngle = RotatePID.angle
        }
    }
}
