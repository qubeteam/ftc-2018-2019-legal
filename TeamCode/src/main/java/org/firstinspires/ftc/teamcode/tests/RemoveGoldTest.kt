package ro.cnmv.qube.ftc.tests

import com.qualcomm.robotcore.eventloop.opmode.Autonomous
import com.qualcomm.robotcore.util.ElapsedTime
import org.firstinspires.ftc.teamcode.autonomy.AutonomyBase
import org.firstinspires.ftc.teamcode.hardware.Hardware
import org.firstinspires.ftc.teamcode.hardware.sensors.TFOD
import org.firstinspires.ftc.teamcode.waitMillis

@Autonomous(name = "Remove Gold Test", group = "Tests")
class RemoveGoldTest : AutonomyBase() {
    val strafeDist = 11.0
    val timer = ElapsedTime()

    override fun preInit() {
    }

    override fun preInitLoop() {
        val mata = tfod.gold
        if(mata != TFOD.Positions.UNKNOWN) {
            goldPosition = mata
            waitMillis(50)
        }
        telemetry.addData("GoldPos", goldPosition)
        telemetry.addLine("READY")
        telemetry.update()
    }

    override fun Hardware.run() {
        land()

        timer.reset()

        removeGold(Basis.SLIDERS)
        tfod.stop()

    }
}
